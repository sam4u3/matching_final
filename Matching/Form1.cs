﻿using Spire.Xls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Matching
{
    public partial class Form1 : Form
    {
        string filename = "";
        DataTable dtExcel=new DataTable();
        DataTable proxies = new DataTable();
       
        public Form1()
        {
            InitializeComponent();


            if (this.WindowState == FormWindowState.Maximized)
            {
                Pan_Crawling.Height = 660;
                Pan_Crawling.Width = 1260;

                Pan_Excel_upload.Width = 1260;
                Pan_Select.Width = 1260;
                panel3.Width = 1260;



                dataGridView1.Height = 550;
                dataGridView1.Width = 700;

                panel5.Height = 550;
                panel5.Width = 500;

            }
          

            
          
        }

        private void dd_country_change(object sender, EventArgs e)
        {
            Cb_Website.Items.Clear();
            Cb_Website.Text = "---Select Website---";
            Pan_Crawling.Visible = false;
            if (cb_Country.SelectedItem.ToString() == "NZ")
            {
                Cb_Website.Items.Add("ascent.co.nz");
                Cb_Website.Items.Add("pricespy.co.nz");
                Cb_Website.Items.Add("aquilatech.co.nz");
            }
            if (cb_Country.SelectedItem.ToString() == "KR")
            {
                //Cb_Website.Items.Add("Gmarket.co.kr");
               // Cb_Website.Items.Add("11st.co.kr");
                Cb_Website.Items.Add("danawa.com");
                Cb_Website.Items.Add("enuri.com");
                Cb_Website.Items.Add("auction.co.kr");
            } 
            if (cb_Country.SelectedItem.ToString() == "AU")
            {
                Cb_Website.Items.Add("ht.com.au");
                //Cb_Website.Items.Add("myshopping.com.au");
                //Cb_Website.Items.Add("officeworks.com.au");
                //Cb_Website.Items.Add("getprice.com.au");
                Cb_Website.Items.Add("Ingram.com");
                Cb_Website.Items.Add("staticice.com.au");
            }
            if (cb_Country.SelectedItem.ToString() == "JP")
            {
                Cb_Website.Items.Add("amazon.co.jp");
                Cb_Website.Items.Add("kakaku.com");
                Cb_Website.Items.Add("yodobashi.com");
                Cb_Website.Items.Add("biccamera.com");
               Cb_Website.Items.Add("nttxstore.jp");
            }
            if (cb_Country.SelectedItem.ToString() == "IND")
            {
                Cb_Website.Items.Add("Flipkart");
                Cb_Website.Items.Add("Snapdeal.com");
                Cb_Website.Items.Add("paytm.in");
                Cb_Website.Items.Add("amazon.in");
            }
            if (cb_Country.SelectedItem.ToString() == "CN")
            {
                Cb_Website.Items.Add("jd.com");
                Cb_Website.Items.Add("suning.com");
                Cb_Website.Items.Add("Amazon.cn");
                //Cb_Website.Items.Add("tmall.com");

            }
            if (cb_Country.SelectedItem.ToString() == "MY")
            {
                //Cb_Website.Items.Add("google.com.my");
                Cb_Website.Items.Add("asashi.com.my");
                //Cb_Website.Items.Add("lelong.com.my");
                //Cb_Website.Items.Add("lowyat.net");
                //Cb_Website.Items.Add("hardwarezone.com.my");
                Cb_Website.Items.Add("lazada.com.my");
                //Cb_Website.Items.Add("FMM_Price_book");
            }
            if (cb_Country.SelectedItem.ToString() == "SG")
            {
               // Cb_Website.Items.Add("videopro.com.sg");
                //Cb_Website.Items.Add("bizgram.com.sg");
                //Cb_Website.Items.Add("hachi.tech");
                //Cb_Website.Items.Add("hardwarezone.com.sg");
                Cb_Website.Items.Add("lazada.sg");
                //Cb_Website.Items.Add("FMM Pricebook");
                //Cb_Website.Items.Add("bizgram.com.sg ");
                //Cb_Website.Items.Add("google.com.sg");
                //Cb_Website.Items.Add("best-bargain-computers.com");
                
            }
            if (cb_Country.SelectedItem.ToString() == "TH")
            {
                //Cb_Website.Items.Add("siamget.com");
                Cb_Website.Items.Add("shop4thai.com");
                Cb_Website.Items.Add("lazada.co.th");
            }
        }
        private void dd_website_Change(object sender, EventArgs e)
        {
            if (Cb_Website.SelectedItem.ToString() != "--- Select Website ---")
            {
                Pan_Excel_upload.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                filename = openFileDialog1.FileName;
                txt_Excel.Text = filename;
            }
        }


       


        private void button2_Click(object sender, EventArgs e)
        {
            dtExcel.Clear();
            string fileExt = Path.GetExtension(filename);

            dtExcel = ReadExcel(filename, fileExt);
            Removeblank();

            label5.Text = "Status : " + dtExcel.Rows.Count + " URLS Found ";
            dataGridView1.DataSource = dtExcel;
            Pan_Crawling.Visible = true;
           ;
        }
        public DataTable ReadExcel(string fileName, string fileExt)
        {
            string conn = string.Empty;
            DataTable dtexcel = new DataTable();
            if (fileExt.CompareTo(".xls") == 0)
                conn = @"provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties='Excel 8.0;HRD=Yes;IMEX=1';"; //for below excel 2007  
            else
                conn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties='Excel 12.0;HDR=NO';"; //for above excel 2007  
            using (OleDbConnection con = new OleDbConnection(conn))
            {
                try
                {
                    OleDbDataAdapter oleAdpt = new OleDbDataAdapter("select * from [Sheet1$]", con); //here we read data from sheet1  
                    oleAdpt.Fill(dtexcel); //fill excel data into dataTable 
                    oleAdpt.Dispose();
                }
                catch { }
                finally
                {


                }
            }
            return dtexcel;
        }
        public void Removeblank()
        {
            foreach (DataRow dr in dtExcel.Rows)
            {
                if (dr["F1"].ToString() == "")
                {

                    dr.Delete();

                }
            }

            dtExcel.AcceptChanges();



        }

        private void btn_auto_Click(object sender, EventArgs e)
        {
            if (Cb_Website.Text == "---Select Website---")
            {
                MessageBox.Show("ERROR : Select WebSite First");
            
            
            }
            if (Cb_Website.SelectedItem == "ascent.co.nz")
            {
                Ascent Ascent_object = new Ascent();
                DataTable Result = Ascent_object.Ascent_auto(dtExcel);
                Save_o_Excel(Result, "Ascent");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
              label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";            
            }
            if (Cb_Website.SelectedItem == "pricespy.co.nz")
            {
                pricebuy_2 pricespy_object = new pricebuy_2();

                DataTable Result = pricespy_object.Pricespy_auto(dtExcel);
                Save_o_Excel(Result, "Ascent");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "aquilatech.co.nz")
            {
                AquilatechNZ aquilatechNZ_object = new AquilatechNZ();

                DataTable Result = aquilatechNZ_object.aquilatech_auto(dtExcel);
                Save_o_Excel(Result, "Aquilatech");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "Gmarket.co.kr")
            {
                Gmarket Gmarket_object = new Gmarket();

                DataTable Result = Gmarket_object.Gmarket_auto(dtExcel);
                Save_o_Excel(Result, "Gmarket");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "11st.co.kr")
            {
                Street11 Street11_object = new Street11();

                DataTable Result = Street11_object.Street11_auto(dtExcel);
                Save_o_Excel(Result, "11street");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "danawa.com")
            {
                Danawa Danawa_object = new Danawa();

                DataTable Result = Danawa_object.Danawa_auto(dtExcel);
                Save_o_Excel(Result, "Danawa");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "enuri.com")
            {
                Enuri Enuri_object = new Enuri();

                DataTable Result = Enuri_object.Enuri_auto(dtExcel);
                Save_o_Excel(Result, "Enuri");
                dataGridView1.DataSource = Result;

                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "auction.co.kr")
            {
                Auction Auction_object = new Auction();

                DataTable Result = Auction_object.Auction_auto(dtExcel);
                Save_o_Excel(Result, "Auction");
                dataGridView1.DataSource = Result;

                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "ht.com.au")
            {
                HT HT_object = new HT();

                DataTable Result = HT_object.HT_auto(dtExcel);
                Save_o_Excel(Result, "HT");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "myshopping.com.au")
            {
                MyShopping_AU MyShopping_object = new MyShopping_AU();

                DataTable Result = MyShopping_object.myshopping_auto(dtExcel);
                Save_o_Excel(Result, "MyShopping");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "officeworks.com.au")
            {
               
                officeworksAU officeworks_object = new officeworksAU();
                DataTable Result = officeworks_object.officeworksAU_auto(dtExcel);
                Save_o_Excel(Result, "officeworksAU");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "getprice.com.au")
            {
                getprice getprice_object = new getprice();

                DataTable Result = getprice_object.getprice_auto(dtExcel);
                Save_o_Excel(Result, "Getprice");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "Ingram.com")
            {
                Ingram Ingram_object = new Ingram();

                DataTable Result = Ingram_object.Ingram_auto(dtExcel);
                Save_o_Excel(Result, "Ingram");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "staticice.com.au")
            {
                staticiceAU staticiceAU_object = new staticiceAU();

                DataTable Result = staticiceAU_object.staticiceAU_auto(dtExcel);
                Save_o_Excel(Result, "staticiceAU");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "amazon.co.jp")
            {
                Amazon_Jp Amazon_object = new Amazon_Jp();
                label5.Text = "Status: Running ";
                DataTable Result = Amazon_object.Amazon_Jp_auto(dtExcel);
                Save_o_Excel(Result, "Amazon_Jp");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "kakaku.com")
            {
                kakaku kakaku_object = new kakaku();

                DataTable Result = kakaku_object.kakaku_auto(dtExcel);
                Save_o_Excel(Result, "kakaku");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "yodobashi.com")
            {
                yodobashi yodobashi_object = new yodobashi();

                DataTable Result = yodobashi_object.yodobashi_auto(dtExcel);
                Save_o_Excel(Result, "yodobashi");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "biccamera.com")
            {
                biccamera biccamera_object = new biccamera();

                DataTable Result = biccamera_object.biccamera_auto(dtExcel);
                Save_o_Excel(Result, "biccamera");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "nttxstore.jp")
            {
                nttxstore nttxstore_object = new nttxstore();

                DataTable Result = nttxstore_object.nttxstore_auto(dtExcel);
                Save_o_Excel(Result, "nttxstore");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "Flipkart")
            {
                Flipkart Flipkart_object = new Flipkart();

                DataTable Result = Flipkart_object.Flipkart_auto(dtExcel);
                Save_o_Excel(Result, "Flipkart");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "Snapdeal.com")
            {
                Snapdeal Snapdeal_object = new Snapdeal();

                DataTable Result = Snapdeal_object.Snapdeal_auto(dtExcel);
                Save_o_Excel(Result, "snapdeal");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "paytm.in")
            {
                Paytm Paytm_object = new Paytm();

                DataTable Result = Paytm_object.Paytm_auto(dtExcel);
                Save_o_Excel(Result, "paytm");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "amazon.in")
            {
                Amazon_In Amazon_In_object = new Amazon_In();

                DataTable Result = Amazon_In_object.Amazon_auto(dtExcel);
                Save_o_Excel(Result, "amazon");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "jd.com")
            {
                JD JD_object = new JD();

                DataTable Result = JD_object.JD_auto(dtExcel);
                Save_o_Excel(Result, "Jd");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "suning.com")
            {
                Suning suning_object = new Suning();

                DataTable Result = suning_object.Suning_auto(dtExcel);
                Save_o_Excel(Result, "suning");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "Amazon.cn")
            {
                AmazonCN AmazonCN_object = new AmazonCN();

                DataTable Result = AmazonCN_object.AmazonCN_auto(dtExcel);
                Save_o_Excel(Result, "Amazon_cn");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "tmall.com")
            {
                Tmall Tmall_object = new Tmall();

                DataTable Result = Tmall_object.Tmall_auto(dtExcel);
                Save_o_Excel(Result, "Tmall");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "videopro.com.sg")
            {
                videopro videopro_object = new videopro();

                DataTable Result = videopro_object.videopro_auto(dtExcel);
                Save_o_Excel(Result, "amazon_invideopro");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "bizgram.com.sg")
            {
                bizgramSg bizgram_object = new bizgramSg();

                DataTable Result = bizgram_object.bizgramSG_auto(dtExcel);
                Save_o_Excel(Result, "bizgramSG");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "google.com.sg")
            {
                //google_sg google_sg_object = new google_sg();

                //DataTable Result = google_sg_object.google_sg_auto(dtExcel);
                //Save_o_Excel(Result, "google_sg");
                //dataGridView1.DataSource = Result;
                //dataGridView1.Refresh();
                //label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "hardwarezone.com.sg")
            {
                //hardwarezone_sg hardwarezone_sg_object = new hardwarezone_sg();

                //DataTable Result = hardwarezone_sg_object.hardwarezone_sg_auto(dtExcel);
                //Save_o_Excel(Result, "hardwarezone_sg");
                //dataGridView1.DataSource = Result;
                //dataGridView1.Refresh();
                //label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "best-bargain-computers.com")
            {
                //best_bargain_computers best_bargain_computers_object = new best_bargain_computers();

                //DataTable Result = best_bargain_computers_object.best_bargain_computers_auto(dtExcel);
                //Save_o_Excel(Result, "best_bargain_computers_sg");
                //dataGridView1.DataSource = Result;
                //dataGridView1.Refresh();
                //label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == " siamget.com")
            {
                //siamget siamget_object = new siamget();

                //DataTable Result = siamget_object.siamget_auto(dtExcel);
                //Save_o_Excel(Result, "siamget");
                //dataGridView1.DataSource = Result;
                //dataGridView1.Refresh();
                //label5.Text = "Status :Completed ("+Result.Rows.Count+" Rows Found)";
            }
            if (Cb_Website.SelectedItem == "shop4thai.com")
            {
                shop4thai shop4thai_object = new shop4thai();

                DataTable Result = shop4thai_object.shop4thai_auto(dtExcel);
                Save_o_Excel(Result, "shop4thai");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "lazada.co.th")
            {
                LazadaTH lazada_th_object = new LazadaTH();

                DataTable Result = lazada_th_object.LazadaTH_auto(dtExcel);
                Save_o_Excel(Result, "lazada_th");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "lazada.com.my")
            {
                LazadaMY lazada_my_object = new LazadaMY();

                DataTable Result = lazada_my_object.LazadaMY_auto(dtExcel);
                Save_o_Excel(Result, "lazada_my");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "hachi.tech")
            {
                hachitechSG hachitechSG_object = new hachitechSG();

                DataTable Result = hachitechSG_object.hachitechSG_auto(dtExcel);
                Save_o_Excel(Result, "hachi.tech");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "lazada.sg")
            {
                LazadaSG LazadaSG_object = new LazadaSG();

                DataTable Result = LazadaSG_object.LazadaSG_auto(dtExcel);
                Save_o_Excel(Result, "lazada_sg");
                dataGridView1.DataSource = Result;
                dataGridView1.Refresh();
                label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
            if (Cb_Website.SelectedItem == "asashi.com.my")
            {
                //asashiMY asahi_object = new asashiMY();

                //DataTable Result = asahi_object.asashiMY_auto(dtExcel);
                //Save_o_Excel(Result, "asashiMY");
                //dataGridView1.DataSource = Result;
                //dataGridView1.Refresh();
                //label5.Text = "Status :Completed (" + Result.Rows.Count + " Rows Found)";
            }
           

        }

        public void Save_o_Excel(DataTable data,string web_site)

    {
        Workbook book = new Workbook();
        Worksheet sheet = book.Worksheets[0];

        sheet.InsertDataTable(data, true, 1, 1);
        book.SaveToFile(txt_out.Text +@"\"+web_site+"_"+ DateTime.Now.ToString("MMMM dd") + ".xls", ExcelVersion.Version2016);

    
    }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                // Check your window state here
                if (m.WParam == new IntPtr(0xF030)) // Maximize event - SC_MAXIMIZE from Winuser.h
                {

                   
                    Pan_Crawling.Height = 660;
                    Pan_Crawling.Width = 1260;

                    Pan_Excel_upload.Width = 1260;
                    Pan_Select.Width = 1260;
                    panel3.Width = 1260;



                    dataGridView1.Height = 550;
                    dataGridView1.Width = 700;

                    panel5.Height = 550;
                    panel5.Width = 500;




                }
                if (m.WParam == new IntPtr(0xF020)) // Maximize event - mini from Winuser.h
                {
                    Pan_Crawling.Height = 562;
                    Pan_Crawling.Width = 1151;

                    Pan_Excel_upload.Width = 1151;
                    Pan_Select.Width = 1151;
                    panel3.Width = 1151;



                    dataGridView1.Height = 463;
                    dataGridView1.Width = 715;
                    panel5.Height = 463;
                    panel5.Width = 403;


                }
                if (m.WParam == new IntPtr(0xF120)) // Maximize event - restore from Winuser.h
                {
                    this.WindowState = FormWindowState.Maximized;
                    if (this.WindowState == FormWindowState.Minimized)
                    {

                        Pan_Crawling.Height = 660;
                        Pan_Crawling.Width = 1260;

                        Pan_Excel_upload.Width = 1260;
                        Pan_Select.Width = 1260;
                        panel3.Width = 1260;



                        dataGridView1.Height = 550;
                        dataGridView1.Width = 700;

                        panel5.Height = 550;
                        panel5.Width = 500;

                    }
                    else
                    {

                        Pan_Crawling.Height = 562;
                        Pan_Crawling.Width = 1151;

                        Pan_Excel_upload.Width = 1151;
                        Pan_Select.Width = 1151;
                        panel3.Width = 1151;



                        dataGridView1.Height =463;
                        dataGridView1.Width = 715;

                        panel5.Height = 463;
                        panel5.Width = 403;

                    }
                    
                    


                }
            }
            base.WndProc(ref m);
        }
       


       
    }
}

﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;

namespace Matching
{
    class yodobashi
    {
        DataTable Results = new DataTable();
        public DataTable yodobashi_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));

                string Productname = dr["F1"].ToString().Replace(" ", "+");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();
            }
            return Results;
        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {
                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {
                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {
                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("Original Price"))
            {
                Results.Columns.Add("Original Price");
            }
            if (!Results.Columns.Contains("Discounted Price"))
            {
                Results.Columns.Add("Discounted Price");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {
            web_driver.Navigate().GoToUrl("https://www.yodobashi.com/?disptyp=01&word=" + product);

            Thread.Sleep(6000);
            return web_driver;
        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {
            string URL = web_driver.Url;
            string product_link = "";

            string productname = "";

            string Originalprice="", Discountedprice = "";
            try
            {

                List<IWebElement> results_tr = web_driver.FindElements(By.ClassName("pInfoArea")).ToList();
                
                
                foreach (IWebElement Tr_ele in results_tr)
                {
                     product_link = "";

                     productname = "";

                     Originalprice = "";
                    Discountedprice = "";

                    try
                    {
                         URL = web_driver.Url;

                        IWebElement Namenhref = Tr_ele.FindElement(By.ClassName("pName")).FindElement(By.TagName("a"));
                         product_link = Namenhref.GetAttribute("href");

                        IWebElement Namef = Namenhref.FindElement(By.TagName("img"));
                         productname = Namef.GetAttribute("alt");

                         Originalprice = "";
                        Discountedprice = "";

                        try
                        {
                            Originalprice = Tr_ele.FindElement(By.ClassName("productPrice")).Text;
                        }
                        catch (Exception er)
                        {
                            Originalprice = Tr_ele.FindElement(By.ClassName("pInfo")).FindElement(By.ClassName("gray")).Text;
                        }



                        Add_Record(URL, productname, product_link, Originalprice, Discountedprice);
                    }
                    catch (Exception e)
                    {
                        Add_Record(URL, productname, product_link, Originalprice, Discountedprice);
                    
                    }

                }
            }
            catch (Exception er)
            {
                Add_Record(URL, productname, product_link, Originalprice, Discountedprice);

            }
            return web_driver;
        }


        private void Add_Record(string URL, string productname, string product_link, string OriginalPrice, string Discountedprice)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["Original Price"] = OriginalPrice;
            New_Record["Discounted Price"] = Discountedprice;

            Results.Rows.Add(New_Record);


        }


        #endregion





    }
}

﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class AquilatechNZ
    {
        DataTable Results = new DataTable();
        public DataTable aquilatech_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));

                string Productname = dr["F1"].ToString().Replace(" ", "%20");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }
        
        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("OriginalPrice"))
            {

                Results.Columns.Add("OriginalPrice");
            }
            if (!Results.Columns.Contains("DiscountedPrice"))
            {

                Results.Columns.Add("DiscountedPrice");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {
            web_driver.Navigate().GoToUrl("https://www.aquilatech.co.nz/ProductList.asp?txtTarget=" + product);
            Thread.Sleep(8000);

            return web_driver;
        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {

            string URL = web_driver.Url;
            string productname = "";
            string product_link = "";
            string Ogprice = "";
            string dsprice = "";
            try
            {
                IWebElement result_div = web_driver.FindElement(By.XPath("//div[@id='pageContent']/div[@class='topLeft']/div[@class='topRight']/div[@class='inner']"));
                if (result_div != null)
                {
                    List<IWebElement> results_tr = result_div.FindElements(By.XPath("./table[@id='productsTable']/tbody/tr[contains(@class,'hideOnMobile')]")).ToList();
                    if (results_tr.Count != 0)
                    {
                        foreach (IWebElement Tr_ele in results_tr)
                        {
                             productname = "";
                             product_link = "";
                             Ogprice = "";
                             dsprice = "";

                            try
                            {
                                IWebElement temp = Tr_ele.FindElement(By.XPath("./th[1]"));
                                if (temp != null)
                                {
                                    if (temp.Text == "#")
                                        continue;
                                }
                            }
                            catch (Exception er)
                            {
                               
                                 productname = Tr_ele.FindElement(By.XPath("./td[2]")).Text;
                                 product_link = Tr_ele.FindElement(By.XPath("./td[2]/a")).GetAttribute("href");
                                 Ogprice = Tr_ele.FindElement(By.XPath("./td[6]")).Text;
                                 dsprice = Tr_ele.FindElement(By.XPath("./td[7]")).Text;
                                Add_Record(URL, productname, product_link, Ogprice, dsprice);
                            }
                        }
                    }
                    else
                    {
                        List<IWebElement> results_tr1 = result_div.FindElements(By.XPath("./table[@cellpadding=3]/tbody/tr")).ToList();
                        foreach (IWebElement Tr_ele in results_tr1)
                        {
                            
                            productname = Tr_ele.FindElement(By.XPath("//div[@id='pageContent']/div[@class='topLeft']/div[@class='topRight']/div[@class='inner']/h2")).Text;
                            string ProductID = Tr_ele.FindElement(By.XPath("//div[@id='pricing']")).Text;
                            int startindex = 0;
                            int endindex = 0;
                            if (ProductID != "" && ProductID.Contains("Aquila ID:"))
                            {
                                startindex = ProductID.IndexOf("Aquila ID:");
                                endindex = ProductID.IndexOf("Vendor:");
                                int len1 = endindex - startindex;
                                ProductID = ProductID.Substring(startindex, len1);
                                string ProdID = ProductID.Replace("\r", "").Replace("\n", "").Replace("Aquila ID: ", "");
                                product_link = "http://www.aquilatech.co.nz/productDetail.asp?idProduct=" + ProdID;
                                Ogprice = Tr_ele.FindElement(By.XPath("./td[3]/div[@id='pricing']/div[@id='yourPrice']/span[2]")).Text;
                                 dsprice = "";
                                Add_Record(URL, productname, product_link, Ogprice, dsprice);
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                Add_Record(URL, productname, product_link, Ogprice, dsprice);
            
            }
            return web_driver;
        }

        #endregion
        private void Add_Record(string URL, string productname, string product_link, string Ogprice, string dsprice)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["OriginalPrice"] = Ogprice;
            New_Record["DiscountedPrice"] = dsprice;


            Results.Rows.Add(New_Record);


        }
    }
}


       

    



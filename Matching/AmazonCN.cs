﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class AmazonCN
    {
        DataTable Results = new DataTable();
        public DataTable AmazonCN_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));

                string Productname = dr["F1"].ToString().Replace(" ", "+");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }


        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("OriginalPrice"))
            {

                Results.Columns.Add("OriginalPrice");
            }
            if (!Results.Columns.Contains("DiscountedPrice"))
            {

                Results.Columns.Add("DiscountedPrice");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {
            web_driver.Navigate().GoToUrl("https://www.amazon.cn/s/ref=nb_sb_noss?&url=search-alias%3Daps&field-keywords=" + product);           
            Thread.Sleep(8000);

            return web_driver;
        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {

            string URL = web_driver.Url;
            string productname = "";
            string product_link = "";

            string Ogprice = "";

            string dsprice = "";
            try
            {
                IWebElement result_div = web_driver.FindElement(By.Id("atfResults"));

                try
                {
                    List<IWebElement> results_tr = result_div.FindElements(By.ClassName("s-result-card-for-container-noborder")).ToList();
                   
                        foreach (IWebElement Tr_ele in results_tr)
                        {
                             productname = "";
                             product_link = "";

                             Ogprice = "";

                             dsprice = "";


                            productname = Tr_ele.FindElement(By.ClassName("s-access-title")).Text;
                            product_link = Tr_ele.FindElement(By.ClassName("s-access-detail-page")).GetAttribute("href");
                            try
                            {
                                Ogprice = Tr_ele.FindElement(By.ClassName("a-color-price")).Text;

                                dsprice = "-";
                            }
                            catch (Exception e)
                            {


                            }
                            Add_Record(URL, productname, product_link, Ogprice, dsprice);

                        }
                }
                catch (Exception e)
                {
                    Add_Record(URL, productname, product_link, Ogprice, dsprice);
                
                }
                
            }
            catch(Exception er)
            {
                Add_Record(URL, productname, product_link, Ogprice, dsprice);
            }
            
            return web_driver;
        }
        private void Add_Record(string URL, string productname, string product_link, string Ogprice, string dsprice)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["OriginalPrice"] = Ogprice;
            New_Record["DiscountedPrice"] = dsprice;


            Results.Rows.Add(New_Record);


        }
    }
}


        #endregion

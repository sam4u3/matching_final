﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matching
{
    class officeworksAU
    {
        DataTable Results = new DataTable();
        public DataTable officeworksAU_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));
                string Productname = dr["F1"].ToString().Replace(" ", "%20");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("OriginalPrice"))
            {

                Results.Columns.Add("OriginalPrice");
            }
            if (!Results.Columns.Contains("DiscountedPrice"))
            {

                Results.Columns.Add("DiscountedPrice");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {

            web_driver.Navigate().GoToUrl("https://www.officeworks.com.au/shop/SearchDisplay?searchTerm=" + product + "&storeId=10151&langId=-1&pageSize=24&beginIndex=0&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&searchSource=Q");
            //web_driver.Navigate().GoToUrl("https://www.officeworks.com.au/shop/SearchDisplay?searchTerm=logitech+Z120+2.0+Speaker&storeId=10151&langId=-1&pageSize=24&beginIndex=0&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&searchSource=Q");
            Thread.Sleep(8000);
            return web_driver;


        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {
              string URL = web_driver.Url;
            string productname = "";
            string product_link = "";
              string Ogprice = "";
                  string dsprice = "";

            try
            {
                IWebElement result_div = web_driver.FindElement(By.XPath("//div[@id='productList']/div[@class='ow-tile-grid__item']"));

                       List<IWebElement> results_tr = result_div.FindElements(By.XPath("./div[@class='ow-product-tile product']")).ToList();
                       
                            foreach (IWebElement Tr_ele in results_tr)
                            {

                                 productname = "";
                                 product_link = "";
                                 Ogprice = "";
                                 dsprice = "";

                                try
                                {

                                     URL = web_driver.Url;

                                     productname = Tr_ele.FindElement(By.XPath("./div[@class='ow-product-tile__inner']/h5")).Text;
                                     product_link = Tr_ele.FindElement(By.XPath("./a")).GetAttribute("href");

                                     Ogprice = Tr_ele.FindElement(By.XPath("div[@class='ow-product-tile__inner']/div[@class='ow-price ow-price--small']/span/div[@class='price-text']/span")).Text;
                                     dsprice = "-";

                                    Add_Record(URL, productname, product_link, Ogprice, dsprice);
                                }
                                catch (Exception e)
                                {
                                    Add_Record(URL, productname, product_link, Ogprice, dsprice);
                                
                                }
                            }
                    
                
            }
            catch(Exception er)
            {
                Add_Record(URL, productname, product_link, Ogprice, dsprice);
            }
            
            return web_driver;
        }



        private void Add_Record(string URL, string productname, string product_link, string Ogprice, string dsprice)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["OriginalPrice"] = Ogprice;
            New_Record["DiscountedPrice"] = dsprice;


            Results.Rows.Add(New_Record);


        }
    }
}


        #endregion








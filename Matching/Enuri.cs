﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class Enuri
    {
        DataTable Results = new DataTable();
        public DataTable Enuri_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));

                string Productname = dr["F1"].ToString().Replace(" ", "%20");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();
            }
            return Results;
        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {
                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {
                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {
                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("Original Price"))
            {
                Results.Columns.Add("Original Price");
            }
            if (!Results.Columns.Contains("Discounted Price"))
            {
                Results.Columns.Add("Discounted Price");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {
            web_driver.Navigate().GoToUrl("http://www.enuri.com/search.jsp?nosearchkeyword=&issearchpage=&searchkind=&es=&c=&ismodelno=false&hyphen_2=false&from=&owd=&keyword=" + product);
            
            Thread.Sleep(8000);
            return web_driver;
        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {
            string URL = web_driver.Url;
            string productname = "";
            string Originalprice = "";
            string Discountedprice = "";

            
            string product_link = "";
            try
            {
                IWebElement result_div = web_driver.FindElementById("listBodyDiv");

                List<IWebElement> results_tr = result_div.FindElements(By.XPath("./li")).ToList();

                foreach (IWebElement Tr_ele in results_tr)
                {
                     productname = "";
                     Originalprice = "";
                     Discountedprice = "";


                     product_link = "";
                    try
                    {
                        productname = Tr_ele.FindElement(By.ClassName("info_area")).FindElement(By.TagName("strong")).Text;
                        string id = Tr_ele.GetAttribute("id");
                        product_link = "";
                        if (id.Contains("plno"))
                        {
                            id = id.Replace("plno_", "");
                            product_link = "http://www.enuri.com/move/Redirect.jsp?type=ex&cmd=move_" + id + "&pl_no=" + id;
                        }
                        else
                        {
                            if (id.Contains("modelno"))
                            {
                                product_link = "http://www.enuri.com/detail.jsp?modelno=10308198" + id.Replace("modelno_", "");
                            }
                        }

                        Originalprice = Tr_ele.FindElement(By.ClassName("price_area")).FindElement(By.TagName("strong")).Text.Replace(",", "");
                        Discountedprice = "";

                        Add_Record(URL, productname, product_link, Originalprice, Discountedprice);
                    }
                    catch (Exception e)
                    {
                        Add_Record(URL, productname, product_link, Originalprice, Discountedprice);
                    
                    }
                }
            }
            catch (Exception er)
            {
                Add_Record(URL, productname, product_link, Originalprice, Discountedprice);
            }
            return web_driver;
        }


        private void Add_Record(string URL, string productname, string product_link, string OriginalPrice, string Discountedprice)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["Original Price"] = OriginalPrice;
            New_Record["Discounted Price"] = Discountedprice;

            Results.Rows.Add(New_Record);


        }


        #endregion




    }
}

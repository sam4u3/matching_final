﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class LazadaMY
    {
        DataTable Results = new DataTable();
        public DataTable LazadaMY_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));

                string Productname = dr["F1"].ToString().Replace(" ", "+");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("OriginalPrice"))
            {

                Results.Columns.Add("OriginalPrice");
            }
            if (!Results.Columns.Contains("DiscountedPrice"))
            {

                Results.Columns.Add("DiscountedPrice");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {

             web_driver.Navigate().GoToUrl("https://www.lazada.com.my/catalog/?q=" + product + "&_keyori=ss&from=input");
             //web_driver.Navigate().GoToUrl("https://www.lazada.co.th/catalog/?q=+HP+S5000+&_keyori=ss&from=input");
            Thread.Sleep(8000);
            return web_driver;


        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {
            string URL = web_driver.Url;
            string productname = "";
            string product_link = "";

            string Ogprice = "";

            string dsprice = "";

            try
            {
                IWebElement result_div = web_driver.FindElement(By.ClassName("c1_t2i"));

                
                        List<IWebElement> results_tr = result_div.FindElements(By.XPath("//div[@class='c2prKC']/div[@class='c3e8SH']")).ToList();
                       

                            foreach (IWebElement Tr_ele in results_tr)
                            {
                                 productname = "";
                                 product_link = "";

                                 Ogprice = "";

                                 dsprice = "";
                                try
                                {

                                     productname = Tr_ele.FindElement(By.ClassName("c16H9d")).FindElement(By.XPath("./a")).Text;
                                     product_link = Tr_ele.FindElement(By.ClassName("c16H9d")).FindElement(By.XPath("./a")).GetAttribute("href");

                                     Ogprice = Tr_ele.FindElement(By.ClassName("c3gUW0")).FindElement(By.XPath("./span")).Text.Replace("RM","");

                                     try
                                     {
                                         dsprice = Tr_ele.FindElement(By.ClassName("c1-B2V")).Text.Replace("RM", ""); ;
                                     }
                                     catch (Exception e) { }

                                    Add_Record(URL, productname, product_link, Ogprice, dsprice);
                                }
                                catch (Exception e)
                                {
                                    Add_Record(URL, productname, product_link, Ogprice, dsprice);
                                
                                }
                            
                }
            }
            catch(Exception er)
            {
                Add_Record(URL, productname, product_link, Ogprice, dsprice);
            }
            
            return web_driver;
        }
        private void Add_Record(string URL, string productname, string product_link, string Ogprice, string dsprice)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["OriginalPrice"] = Ogprice;
            New_Record["DiscountedPrice"] = dsprice;


            Results.Rows.Add(New_Record);


        }
    }
}


        #endregion
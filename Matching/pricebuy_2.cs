﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class pricebuy_2
    {

        DataTable Results = new DataTable();
        public DataTable Pricespy_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));

                string Productname = dr["F1"].ToString().Replace(" ", "%20");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("OriginalPrice"))
            {

                Results.Columns.Add("OriginalPrice");
            }
            if (!Results.Columns.Contains("DiscountedPrice"))
            {

                Results.Columns.Add("DiscountedPrice");
            }

        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {

            web_driver.Navigate().GoToUrl("https://pricespy.co.nz/#rparams=ss=" + product);

            Thread.Sleep(6000);
            return web_driver;


        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {

            string URL = web_driver.Url;
            string productname = "";
            string product_link = "";
            string price = "";


            try
            {
                IWebElement result_div = web_driver.FindElementById("search_results_product");

                List<IWebElement> results_tr = result_div.FindElements(By.XPath("./table/tbody/tr")).ToList();

                foreach (IWebElement Tr_ele in results_tr)
                {

                     URL = web_driver.Url;
                     productname = "";
                     product_link = "";
                     price = "";

                    try
                    {
                         URL = web_driver.Url;
                         productname = Tr_ele.FindElement(By.XPath("./td[1]")).Text;
                         product_link = "";
                        try
                        {
                            product_link = Tr_ele.FindElement(By.XPath("./td/a[2]")).GetAttribute("href");
                        }
                        catch { }
                         price = Tr_ele.FindElement(By.XPath("td[2]")).Text;

                        Add_Record(URL, productname, product_link, price);
                    }
                    catch (Exception e)
                    {

                        Add_Record(URL, productname, product_link, price);
                    
                    }
                }
            }
            catch (Exception er)
            {
                Add_Record(URL, productname, product_link, price);
            }



            return web_driver;
        }


        private void Add_Record(string URL, string productname, string product_link, string price)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["OriginalPrice"] = price;

            Results.Rows.Add(New_Record);


        }


        #endregion



    }
}

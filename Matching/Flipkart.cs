﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class Flipkart
    {
        DataTable Results = new DataTable();
        public DataTable Flipkart_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));

                string Productname = dr["F1"].ToString().Replace(" ", "%20");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("Original_Price"))
            {
                Results.Columns.Add("Original_Price");
            }
            if (!Results.Columns.Contains("Discounted_Price"))
            {
                Results.Columns.Add("Discounted_Price");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {

            web_driver.Navigate().GoToUrl("https://www.flipkart.com/search?q=" + product + "&marketplace=FLIPKART&otracker=start&as-show=off&as=off");

            Thread.Sleep(8000);
            return web_driver;


        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {

            string URL = web_driver.Url;

            string productname = "";
            string product_link = "";

            string Disc_Price = "";
            string Og_Price = "";
            try
            {
                IWebElement container = web_driver.FindElement(By.XPath("//div[@class='_1HmYoV _35HD7C col-10-12']"));
                //IWebElement result_div = container[2];


                List<IWebElement> results_tr = container.FindElements(By.ClassName("_1R0K0g")).ToList();

                foreach (IWebElement Tr_ele in results_tr)
                {
                     productname = "";
                     product_link = "";

                     Disc_Price = "";
                     Og_Price = "";

                    try
                    {
                        productname = Tr_ele.FindElement(By.ClassName("_2cLu-l")).Text;
                        product_link = Tr_ele.FindElement(By.ClassName("Zhf2z-")).GetAttribute("href");

                        Disc_Price = Tr_ele.FindElement(By.ClassName("_1vC4OE")).Text;
                        Og_Price = Tr_ele.FindElement(By.ClassName("_3auQ3N")).Text;

                        Add_Record(URL, productname, product_link, Og_Price, Disc_Price);
                    }
                    catch (Exception e)
                    {
                        Add_Record(URL, productname, product_link, Og_Price, Disc_Price);
                    
                    }

                }
            }
            catch (Exception er)
            {
                Add_Record(URL, productname, product_link, Og_Price, Disc_Price);
            }


            return web_driver;
        }


        private void Add_Record(string URL, string productname, string product_link, string Og_Price, string Disc_Price)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["Original_Price"] = Og_Price;
            New_Record["Discounted_Price"] = Disc_Price;

            Results.Rows.Add(New_Record);


        }


        #endregion




    }
}

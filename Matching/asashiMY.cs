﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class asashiMY
    {
        DataTable Results = new DataTable();
        public DataTable asashiMY_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy =
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));
                string Productname = dr["F1"].ToString().Replace(" ", "+");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("OriginalPrice"))
            {

                Results.Columns.Add("OriginalPrice");
            }
            if (!Results.Columns.Contains("DiscountedPrice"))
            {

                Results.Columns.Add("DiscountedPrice");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {

            // web_driver.Navigate().GoToUrl("https://www.lazada.co.th/catalog/?q=" + product + "&_keyori=ss&from=input");
            web_driver.Navigate().GoToUrl("http://www.asashi.com.my/");
            IWebElement searchtxtElement = web_driver.FindElement(By.Id("searchText"));
            searchtxtElement.SendKeys(product);
            //searchtxtElement.SendKeys("s23");
            searchtxtElement.Submit();
            Thread.Sleep(8000);
            return web_driver;
            ////div[@class="headerTop"]/div[@class="headerright"]/div[@id="search"]/form/input
            

        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {

            string URL = web_driver.Url;
            string productname = "";
            string dsprice = "";
            string product_link = "";
            string Ogprice = "";
            try
            {
                IWebElement result_div = web_driver.FindElement(By.ClassName("searchResultsGrid"));

               
                        List<IWebElement> results_tr = result_div.FindElements(By.XPath("./tbody/tr/td")).ToList();

                        foreach (IWebElement Tr_ele in results_tr)
                        {

                             productname = "";
                             dsprice = "";
                             product_link = "";
                             Ogprice = "";

                            productname = Tr_ele.FindElement(By.ClassName("prodItemName")).Text;
                            dsprice = "-";
                            product_link = Tr_ele.FindElement(By.ClassName("searchProdGridCol1")).FindElement(By.XPath("./a")).GetAttribute("href");
                            //code to hiot product and get price
                            ChromeDriver web_driver1 = null;
                            ChromeOptions options1 = new ChromeOptions();
                            web_driver1 = new ChromeDriver(options1);
                            try
                            {

                                web_driver1.Navigate().GoToUrl(product_link);
                                Thread.Sleep(6000);
                                Ogprice = web_driver1.FindElement(By.ClassName("viewProdPrice")).FindElement(By.XPath("./span[2]")).Text;
                            }
                            catch (Exception e)
                            {
                                web_driver1.Close();
                                web_driver1.Quit();

                            }
                            Add_Record(URL, productname, product_link, Ogprice, dsprice);
                            //return web_driver1;

                        }
            }
            catch(Exception er)
            {
                Add_Record(URL, productname, product_link, Ogprice, dsprice);
            }
            
            return web_driver;
        }
        private void Add_Record(string URL, string productname, string product_link, string Ogprice, string dsprice)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["OriginalPrice"] = Ogprice;
            New_Record["DiscountedPrice"] = dsprice;


            Results.Rows.Add(New_Record);


        }
    }
}


        #endregion

﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class Danawa
    {
        DataTable Results = new DataTable();
        public DataTable Danawa_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));

                string Productname = dr["F1"].ToString().Replace(" ", "%20");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("Original Price"))
            {

                Results.Columns.Add("Original Price");
            }
            if (!Results.Columns.Contains("Discounted Price"))
            {

                Results.Columns.Add("Discounted Price");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {

            web_driver.Navigate().GoToUrl("http://search.danawa.com/dsearch.php?query=" + product + "&originalQuery=" + product + "&tab=main&volumeType=allvs&page=1&limit=60&sort=saveDESC&list=list&boost=true&addDelivery=N");

            Thread.Sleep(8000);
            return web_driver;


        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {

            string URL = web_driver.Url;

            string productname = "";
            string product_link = "";

            string OriginalPrice = "";
            //string DiscountedPrice = Tr_ele.FindElement(By.ClassName("product-list__price")).Text;
            string DiscountedPrice = "";
            try
            {
                IWebElement result_div = web_driver.FindElementByClassName("product_list");

                List<IWebElement> results_tr = result_div.FindElements(By.XPath("./li")).ToList();

                foreach (IWebElement Tr_ele in results_tr)
                {
                     productname = "";
                     product_link = "";

                     OriginalPrice = "";
                    
                     DiscountedPrice = "";

                    try
                    {
                        productname = Tr_ele.FindElement(By.XPath("./div[@class='prod_main_info']/div[@class='prod_info']/p")).Text;
                        product_link = Tr_ele.FindElement(By.XPath("./div[@class='prod_main_info']/div[@class='prod_info']/p/a")).GetAttribute("href");

                        OriginalPrice = Tr_ele.FindElement(By.ClassName("price_sect")).FindElement(By.TagName("strong")).Text.Replace(",", "");
                        //string DiscountedPrice = Tr_ele.FindElement(By.ClassName("product-list__price")).Text;
                        DiscountedPrice = "";
                        Add_Record(URL, productname, product_link, OriginalPrice, DiscountedPrice);
                    }
                    catch (Exception e)
                    {
                        Add_Record(URL, productname, product_link, OriginalPrice, DiscountedPrice);
                    }
                }
            }
            catch (Exception er)
            {

                Add_Record(URL, productname, product_link, OriginalPrice, DiscountedPrice);
            }

            return web_driver;
        }


        private void Add_Record(string URL, string productname, string product_link, string OriginalPrice, string DiscountedPrice)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["Original Price"] = OriginalPrice;
            New_Record["Discounted Price"] = DiscountedPrice;

            Results.Rows.Add(New_Record);


        }
    }
}


        #endregion








﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class shop4thai
    {
        DataTable Results = new DataTable();
        public DataTable shop4thai_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));
                string Productname = dr["F1"].ToString().Replace(" ", "%20");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("Original_Price"))
            {
                Results.Columns.Add("Original_Price");
            }
            if (!Results.Columns.Contains("Discounted_Price"))
            {
                Results.Columns.Add("Discounted_Price");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {

            web_driver.Navigate().GoToUrl("https://www.shop4thai.com/th/product/?q=" + product);

            Thread.Sleep(8000);
            return web_driver;


        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {
            try
            {
                IWebElement container = web_driver.FindElement(By.Id("main_content"));

                List<IWebElement> results_tr = container.FindElements(By.XPath("//*[@id='main_content']/table/tbody/tr/td/div/table/tbody/tr/td/table[2]/tbody/tr")).ToList();
                int cnt = 1;
                if (results_tr.Count != 0)
                {
                    foreach (IWebElement Tr_ele in results_tr)
                    {
                        if ((cnt%2) != 0)
                        {
                            cnt++;
                            continue;
                        }
                        string URL = web_driver.Url;

                        string productname = Tr_ele.FindElement(By.XPath("./td[2]/a")).GetAttribute("title");
                        string product_link = Tr_ele.FindElement(By.XPath("./td[2]/a")).GetAttribute("href");

                        string Og_Price = Tr_ele.FindElement(By.XPath("./td[5]")).Text;
                        string Disc_Price = ""; // Tr_ele.FindElement(By.ClassName("product-price")).Text;

                        Add_Record(URL, productname, product_link, Og_Price, Disc_Price);
                        cnt++;
                    }
                }
                else 
                {
                    try
                    {
                        string URL = web_driver.Url;

                        string productname = container.FindElement(By.XPath("//*[@id='main_content']/table/tbody/tr/td/table[1]/tbody/tr/td[2]/h1")).Text; ;
                        string product_link = web_driver.FindElement(By.ClassName("addthis_sharing_toolbox")).GetAttribute("data-url"); //container.FindElement(By.XPath("./td[2]/a")).GetAttribute("href");
                        string Og_Price = "", Disc_Price = "";
                        try
                        {
                            Og_Price = container.FindElement(By.XPath("//*[@id='main_content']/table/tbody/tr/td/table[3]/tbody/tr/td[2]/table/tbody/tr[1]/td[2]/font/strike/font/font")).Text;
                        }
                        catch(Exception opr)
                        {}
                        try
                        {
                            Disc_Price = container.FindElement(By.XPath("//*[@id='main_content']/table/tbody/tr/td/table[3]/tbody/tr/td[2]/table/tbody/tr[2]/td[2]/b/font/font/font")).Text; // Tr_ele.FindElement(By.ClassName("product-price")).Text;
                        }
                        catch (Exception dpr)
                        { }

                        Add_Record(URL, productname, product_link, Og_Price, Disc_Price);
                    }
                    catch (Exception err) { }
                }
            }
            catch (Exception er)
            {

            }


            return web_driver;
        }


        private void Add_Record(string URL, string productname, string product_link, string Og_Price, string Disc_Price)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["Original_Price"] = Og_Price;
            New_Record["Discounted_Price"] = Disc_Price;

            Results.Rows.Add(New_Record);


        }


        #endregion




    }
}

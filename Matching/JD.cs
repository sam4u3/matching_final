﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class JD
    {
        DataTable Results = new DataTable();
        public DataTable JD_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                var options = new ChromeOptions();
                options.AddArgument("-no-sandbox");
                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;

                Proxy proxy = new Proxy();
                proxy.Kind = ProxyKind.Manual;
                proxy.IsAutoDetect = false;
                proxy.HttpProxy = "216.227.130.13:80";
                proxy.SslProxy = "216.227.130.13:80";
                options.Proxy = proxy;
                options.AddArgument("ignore-certificate-errors");

                web_driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(3));


                string Productname = dr["F1"].ToString().Replace(" ", "%20");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("Original_Price"))
            {
                Results.Columns.Add("Original_Price");
            }
            if (!Results.Columns.Contains("Discounted_Price"))
            {
                Results.Columns.Add("Discounted_Price");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {

            web_driver.Navigate().GoToUrl("http://search.jd.com/Search?keyword=" + product);
            IJavaScriptExecutor js = (IJavaScriptExecutor)web_driver;
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
            //.executeScript("window.scrollTo(0, document.body.scrollHeight)");

            Thread.Sleep(8000);
            return web_driver;
        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {
            string URL = web_driver.Url;

            string productname = "";
            string product_link = "";

            string Og_Price = "";
            string Disc_Price = ""; 

            try
            {
                IWebElement container = web_driver.FindElement(By.ClassName("gl-warp"));

                List<IWebElement> results_tr = container.FindElements(By.ClassName("gl-i-wrap")).ToList();

                foreach (IWebElement Tr_ele in results_tr)
                {
                     productname = "";
                     product_link = "";

                     Og_Price = "";
                     Disc_Price = ""; 
                    try
                    {
                        productname = Tr_ele.FindElement(By.ClassName("p-name-type-2")).Text;
                        product_link = Tr_ele.FindElement(By.ClassName("p-name-type-2")).FindElement(By.XPath("./a")).GetAttribute("href");

                        Og_Price = Tr_ele.FindElement(By.XPath("./div[@class='p-price']/strong/i")).Text;
                        Disc_Price = ""; 

                        Add_Record(URL, productname, product_link, Og_Price, Disc_Price);
                    }
                    catch (Exception e)
                    {
                        Add_Record(URL, productname, product_link, Og_Price, Disc_Price);
                    }

                }
            }
            catch (Exception er)
            {
                Add_Record(URL, productname, product_link, Og_Price, Disc_Price);
            }


            return web_driver;
        }


        private void Add_Record(string URL, string productname, string product_link, string Og_Price, string Disc_Price)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["Original_Price"] = Og_Price;
            New_Record["Discounted_Price"] = Disc_Price;

            Results.Rows.Add(New_Record);


        }


        #endregion




    }
}

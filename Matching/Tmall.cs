﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matching
{
    class Tmall
    {
        DataTable Results = new DataTable();
        public DataTable Tmall_auto(DataTable Input)
        {
            foreach (DataRow dr in Input.Rows)
            {
                ChromeDriver web_driver = null;
                ChromeOptions options = new ChromeOptions();
                web_driver = new ChromeDriver(options);

                string Productname = dr["F1"].ToString().Replace(" ", "%20");

                Search_product(web_driver, Productname);

                Create_Columns();

                Read_result(web_driver);


                web_driver.Close();
                web_driver.Quit();


            }
            return Results;


        }

        private void Create_Columns()
        {
            if (!Results.Columns.Contains("Search URL"))
            {

                Results.Columns.Add("Search URL");
            }
            if (!Results.Columns.Contains("Product URL"))
            {

                Results.Columns.Add("Product URL");
            }
            if (!Results.Columns.Contains("Product Name"))
            {

                Results.Columns.Add("Product Name");
            }
            if (!Results.Columns.Contains("Original_Price"))
            {
                Results.Columns.Add("Original_Price");
            }
            if (!Results.Columns.Contains("Discounted_Price"))
            {
                Results.Columns.Add("Discounted_Price");
            }
        }


        #region Logic to navigate on search result page
        public ChromeDriver Search_product(ChromeDriver web_driver, string product)
        {

            web_driver.Navigate().GoToUrl("https://www.snapdeal.com/search?keyword=" + product + "&santizedKeyword=&catId=&categoryId=0&suggested=false&vertical=&noOfResults=20&clickSrc=go_header&lastKeyword=&prodCatId=&changeBackToAll=false&foundInAll=false&categoryIdSearched=&cityPageUrl=&categoryUrl=&url=&utmContent=&dealDetail=&sort=rlvncy");

            Thread.Sleep(8000);
            return web_driver;


        }

        #endregion

        #region Logic to get result information from result page

        public ChromeDriver Read_result(ChromeDriver web_driver)
        {
            try
            {
                IWebElement container = web_driver.FindElement(By.Id("products"));

                List<IWebElement> results_tr = container.FindElements(By.XPath("./section/div[contains(@class,'product-tuple-listing')]")).ToList();

                foreach (IWebElement Tr_ele in results_tr)
                {
                    string URL = web_driver.Url;

                    string productname = Tr_ele.FindElement(By.ClassName("product-title")).Text;
                    string product_link = Tr_ele.FindElement(By.XPath("./div[@class='product-tuple-description ']/div/a")).GetAttribute("href");

                    string Og_Price = Tr_ele.FindElement(By.XPath("//div[contains(@class,'marR10')]/span[contains(@class,'-desc-price')]")).Text;
                    string Disc_Price = Tr_ele.FindElement(By.ClassName("product-price")).Text;

                    Add_Record(URL, productname, product_link, Og_Price, Disc_Price);

                }
            }
            catch (Exception er)
            {

            }


            return web_driver;
        }


        private void Add_Record(string URL, string productname, string product_link, string Og_Price, string Disc_Price)
        {

            DataRow New_Record = Results.NewRow();

            New_Record["Search URL"] = URL;
            New_Record["Product URL"] = product_link;
            New_Record["Product Name"] = productname;
            New_Record["Original_Price"] = Og_Price;
            New_Record["Discounted_Price"] = Disc_Price;

            Results.Rows.Add(New_Record);


        }


        #endregion




    }
}
